# Pelican Plugin – Gravatar Plus

Just like the [regular Pelican Gravatar Plugin](https://pelican.readthedocs.org/en/latest/plugins.html#gravatar) but creates a Gravatar object stored in the `author_gravatar` context variable that gives you access to a few more things.

By the way: access the variable in your `article.html` template using `{{ article.author_gravatar }}`.